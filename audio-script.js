const audio = new Audio("/ressources/jazzy-abstract-beat-11254.mp3");
audio.autoplay;
audio.volume = 0.1;

$(document).ready(function() {
    $('.trigger').on('click', function () {
        if (audio.paused == false) {
            audio.pause();
            $('.bi-play-fill').show();
            $('.bi-pause').hide();
            $('.music-card').removeClass('playing');
        } else {
            audio.play();
            $('.bi-pause').show();
            $('.bi-play-fill').hide();
            $('.music-card').addClass('playing');
        }
    });

});
